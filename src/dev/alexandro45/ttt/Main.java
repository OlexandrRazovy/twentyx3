package dev.alexandro45.ttt;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;

public class Main {

    private long work_time = 1000 * 60 * 20; //twenty minutes
    private long free_time = 1000 * 20; //twenty seconds

    Main() {
        while (true) {
            try {
                Thread.sleep(work_time);
                new Notification("TwentyX3", "Time to recreation!").notif();
                playSound(new File("appointed.wav"));
                Thread.sleep(free_time);
                new Notification("TwentyX3", "Time to work!").notif();
                playSound(new File("intuition.wav"));
            } catch (InterruptedException e) {
                e.printStackTrace();//19.36
            }
        }
    }

    private synchronized void playSound(final File url) {
        new Thread(() -> {
            try {
                Clip clip = AudioSystem.getClip();
                AudioInputStream inputStream = AudioSystem.getAudioInputStream(url);
                clip.open(inputStream);
                clip.start();
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }).start();
    }

    public static void main(String[] args) {
        new Main();
    }
}
