package dev.alexandro45.ttt;

import java.io.IOException;

public class Notification {

    private final String COMMAND = "notify-send";
    private String title;
    private String text;
    private String icon;

    public Notification() {}

    public Notification(String text) {
        this.text = text;
    }

    public Notification(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public Notification(String title, String text, String icon) {
        this.title = title;
        this.text = text;
        this.icon = icon;
    }

    public Notification setTitle(String title) {
        this.title = title;
        return this;
    }

    public Notification setText(String text) {
        this.text = text;
        return this;
    }

    public Notification setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    public void notif() {
        if (text == null) return;
        String[] cmdarray = new String[2 + (title != null ? 1 : 0) + (icon != null ? 2 : 0)];
        cmdarray[0] = COMMAND;
        if (icon != null) {
            cmdarray[1] = "-i";
            cmdarray[2] = icon;

            if (title != null) {
                cmdarray[3] = title;
                cmdarray[4] = text;
            } else {
                cmdarray[3] = text;
            }
        } else if (title != null){
            cmdarray[1] = title;
            cmdarray[2] = text;
        } else {
            cmdarray[1] = text;
        }

        try {
            Runtime.getRuntime().exec(cmdarray);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
